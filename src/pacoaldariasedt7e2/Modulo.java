package pacoaldariasedt7e2;

import java.util.ArrayList;

public class Modulo {

   private String Id;
   private String nombre;
   private ArrayList<Profesor> profesores;
   private Ciclo ciclo;

   public Modulo() {
      profesores = new ArrayList<Profesor>();

   }

   /**
    * @return the Id
    */
   public String getId() {
      return Id;
   }

   /**
    * @param Id the Id to set
    */
   public void setId(String Id) {
      this.Id = Id;
   }

   /**
    * @return the nombre
    */
   public String getNombre() {
      return nombre;
   }

   /**
    * @param nombre the nombre to set
    */
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }

   /**
    * @return the Profesores
    */
   public ArrayList<Profesor> getProfesores() {
      return profesores;
   }

   /**
    * @param Profesores the Profesores to set
    */
   public void setProfesores(ArrayList<Profesor> Profesores) {
      this.profesores = Profesores;
   }

   /**
    * @return the ciclo
    */
   public Ciclo getCiclo() {
      return ciclo;
   }

   /**
    * @param ciclo the ciclo to set
    */
   public void setCiclo(Ciclo ciclo) {
      this.ciclo = ciclo;
   }

   public String toString() {
      return this.nombre;
   }

}
