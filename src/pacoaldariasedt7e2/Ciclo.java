package pacoaldariasedt7e2;

import java.util.ArrayList;

public class Ciclo {

   private String id;

   private String nombre;

   private ArrayList<Modulo> modulos;

   public Ciclo() {
      modulos = new ArrayList<Modulo>();

   }

   /**
    * @return the id
    */
   public String getId() {
      return id;
   }

   /**
    * @param id the id to set
    */
   public void setId(String id) {
      this.id = id;
   }

   /**
    * @return the nombre
    */
   public String getNombre() {
      return nombre;
   }

   /**
    * @param nombre the nombre to set
    */
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }

   /**
    * @return the modulos
    */
   public ArrayList<Modulo> getModulos() {
      return modulos;
   }

   /**
    * @param modulos the modulos to set
    */
   public void setModulos(ArrayList<Modulo> modulos) {
      this.modulos = modulos;
   }

   public void setModulo(Modulo modulo) {
      modulos.add(modulo);
   }

   public String toString() {
      return this.nombre;
   }

}
