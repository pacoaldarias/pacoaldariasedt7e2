package pacoaldariasedt7e2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ProfesorTitular extends Profesor {

   // Constructor ejemplo aprenderaprogramar.com
   public ProfesorTitular(String nombre, String apellidos, int edad, String id) {

      super(nombre, apellidos, edad, id);
   }

   public float importeNomina() {
      return 30f * 43.20f;
   }  //Método abstracto sobreescrito en esta clase

} //Cierre de la clase
