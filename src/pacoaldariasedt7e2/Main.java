package pacoaldariasedt7e2;

import java.util.ArrayList;
import java.util.Calendar;

public class Main {

   public static void main(String[] Args) {

      Calendar fecha1 = Calendar.getInstance();

      fecha1.set(2019, 10, 22); //Los meses van de 0 a 11, luego 10 representa noviembre

      // Creando ProfesorInterino
      ProfesorInterino pi1 = new ProfesorInterino("José", "Hernández López", 45, "45221887-K", fecha1);

      ProfesorInterino pi2 = new ProfesorInterino("Andrés", "Moltó Parra", 87, "72332634-L", fecha1);

      ProfesorInterino pi3 = new ProfesorInterino("José", "Ríos Mesa", 76, "34998128-M", fecha1);

      // Creando Titulares
      ProfesorTitular pt1 = new ProfesorTitular("Juan", "Pérez Pérez", 23, "73-K");

      ProfesorTitular pt2 = new ProfesorTitular("Alberto", "Centa Mota", 49, "88-L");

      ProfesorTitular pt3 = new ProfesorTitular("Alberto", "Centa Mota", 49, "81-F");

      // Creando Destino
      Destino destino1 = new Destino();
      destino1.setId("1");
      destino1.setNombre("Ceed");
            
      destino1.getListinProfesores().add(pt1);
      pt1.setDestino(destino1);

      // Creando Interfaces
      IBD ibd = new Mysql();
      ibd.grabar(pi1);

      ibd = new Oracle();
      ibd.grabar(pi1);

      // Creando Ciclo y Modulos
      Ciclo ciclo1 = new Ciclo();
      ciclo1.setId("1");
      ciclo1.setNombre("Daw");

      Modulo modulo1 = new Modulo();
      modulo1.setId("1");
      modulo1.setNombre("ED");
      modulo1.setCiclo(ciclo1);

      ciclo1.setModulo(modulo1);
      pt1.getModulos().add(modulo1);
      modulo1.getProfesores().add(pt1);

      // Creando Departamento
      Departamento departamento1 = new Departamento();
      departamento1.setId("1");
      departamento1.setNombre("Informática");
      departamento1.getProfesores().add(pt1);
      pt1.setDepartamento(departamento1);
      
      
// Creando listin
      ListinProfesores listinProfesorado = new ListinProfesores();

      listinProfesorado.addProfesor(pi1);
      listinProfesorado.addProfesor(pi2);
      listinProfesorado.addProfesor(pi3);

      listinProfesorado.addProfesor(pt1);
      listinProfesorado.addProfesor(pt2);
      listinProfesorado.addProfesor(pt3);

      listinProfesorado.imprimirListin();

      System.out.println("El importe de las nóminas del profesorado que consta en el listín es "
              + listinProfesorado.importeTotalNominaProfesorado() + " euros");
   }
}   //Cierre del main y cierre de la clase
