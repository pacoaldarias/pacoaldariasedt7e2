package pacoaldariasedt7e2;

import java.util.ArrayList;

public class Destino {

   private String id;

   private String nombre;

   private ArrayList<Profesor> profesores;

   
   public Destino() {
      profesores = new ArrayList<Profesor>();
   } 
   
   /**
    * @return the id
    */
   public String getId() {
      return id;
   }

   /**
    * @param id the id to set
    */
   public void setId(String id) {
      this.id = id;
   }

   /**
    * @return the listinProfesores
    */
   public ArrayList<Profesor> getListinProfesores() {
      return profesores;
   }

   /**
    * @param listinProfesores the listinProfesores to set
    */
   public void setListinProfesores(ArrayList<Profesor> listinProfesores) {
      this.profesores = listinProfesores;
   }

   /**
    * @return the nombre
    */
   public String getNombre() {
      return nombre;
   }

   /**
    * @param nombre the nombre to set
    */
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }

}
