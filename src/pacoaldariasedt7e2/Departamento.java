package pacoaldariasedt7e2;

import java.util.ArrayList;

public class Departamento {

   private String id;
   
   private String nombre;
    
   private ArrayList<Profesor> profesores;

   
      public Departamento() {
      profesores = new ArrayList<Profesor>();
   } 
   
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the Profesores
     */
    public ArrayList<Profesor> getProfesores() {
        return profesores;
    }

    /**
     * @param Profesores the Profesores to set
     */
    public void setProfesores(ArrayList<Profesor> Profesores) {
        this.profesores = Profesores;
    }

}
