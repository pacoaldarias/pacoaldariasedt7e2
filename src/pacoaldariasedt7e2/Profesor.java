package pacoaldariasedt7e2;

import java.util.ArrayList;

public abstract class Profesor extends Persona {

   // Campo de la clase ejemplo aprenderaprogramar.com
   private String IdProfesor;
   private Destino destino;
   private Departamento departamento;
   private ArrayList<Modulo> modulos; // Modulos imparte

   // Constructores
   public Profesor() {
      super();
      IdProfesor = "Unknown";
      modulos = new ArrayList<Modulo>();
      destino = null;
   }

   public Profesor(String nombre, String apellidos, int edad, String id) {
      super(nombre, apellidos, edad);
      IdProfesor = id;
      modulos = new ArrayList<Modulo>();
      destino = null;
   }

   // Métodos
   public void setIdProfesor(String IdProfesor) {
      this.IdProfesor = IdProfesor;
   }

   public String getIdProfesor() {
      return IdProfesor;
   }

   public void mostrarDatos() {

      System.out.println("Datos Profesor. Profesor de nombre: " + getNombre() + " "
              + getApellidos() + " con Id de profesor: " + getIdProfesor());
   }

   public String toString() {
      return super.toString().concat(" -IdProfesor: ").concat(getIdProfesor());
   }

   abstract public float importeNomina();  // Método abstracto

   /**
    * @return the IdDestino
    */
   public Destino getDestino() {
      return destino;
   }

   /**
    * @param IdDestino the IdDestino to set
    */
   public void setDestino(Destino destino) {
      this.destino = destino;
   }

   public ArrayList<Modulo> getModulos() {
      return modulos;
   }

   /**
    * @param modulos the modulos to set
    */
   public void setModulos(ArrayList<Modulo> modulos) {
      this.modulos = modulos;
   }

    /**
     * @return the departamento
     */
    public Departamento getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

} //Cierre de la clase
